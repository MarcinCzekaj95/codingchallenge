#include "Input.h"

bool Input::s_isGoingToRight = false;
bool Input::s_isGoingToLeft = false;
bool Input::s_isGoingToUp = false;
bool Input::s_isGoingToDown = false;
bool Input::s_horizontalMove = true;
bool Input::s_verticalMove = false;

void Input::buttonsClick(sf::RenderWindow& p_window)
{
    sf::Event event;
    while (p_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            p_window.close();
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
            p_window.close();
        if(s_verticalMove)
        {
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::D || event.key.code == sf::Keyboard::Right)
                {
                    s_isGoingToRight = true;
                    s_horizontalMove = true;
                    s_verticalMove = false;
                }
                if (event.key.code == sf::Keyboard::A || event.key.code == sf::Keyboard::Left)
                {
                    s_isGoingToLeft = true;
                    s_horizontalMove = true;
                    s_verticalMove = false;
                }
            }
        }
        else if(s_horizontalMove)
        {
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
                {
                    s_isGoingToUp = true;
                    s_horizontalMove = false;
                    s_verticalMove = true;
                }
                if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
                {
                    s_isGoingToDown = true;
                    s_horizontalMove = false;
                    s_verticalMove = true;
                }
            }
        }
    }
}
