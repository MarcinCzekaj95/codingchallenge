#include "Circle.h"
#include <time.h>


Circle::Circle(float p_radius, float p_posX, float p_posY)
{
    m_shape.setRadius(p_radius);
    srand(time(NULL));
    int l_widonwWidth = GameWindow::getWitdth();
    int l_windowHeight = (GameWindow::getHeight() - GameWindow::getSidebarSize());
    m_shape.setPosition(rand() % (l_widonwWidth / 20) * 10, 
                        (rand() % (l_windowHeight / 20) * 10) + GameWindow::getSidebarSize());
}

void Circle::changePosition()
{
    srand(time(NULL));
    int l_widonwWidth = GameWindow::getWitdth();
    int l_windowHeight = (GameWindow::getHeight() - GameWindow::getSidebarSize());
    m_shape.setPosition(rand() % (l_widonwWidth / 20) * 10,
                        (rand() % (l_windowHeight / 20) * 10) + GameWindow::getSidebarSize());
}

sf::CircleShape Circle::getShape() const
{
    return m_shape;
}

sf::Vector2f Circle::getPosition() const
{
    return m_shape.getPosition();
}
