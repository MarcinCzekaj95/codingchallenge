#pragma once
#include <vector>
#include <utility>  
#include "Square.h"
#include "Circle.h"
#include "GameWindow.h"
class Snake
{
public:
    Snake(float p_width, float p_height);

    void update(Circle& p_food);

    void setDirection(float x, float y);

    void savePrevoiusPosition(sf::Vector2f p_position, sf::Vector2f p_direction);

    std::vector<Square> getBlocks() const;

    Square getRootBlock() const;

    sf::Vector2f getRootBlockPosition() const;

    void setCollision(bool p_isCollision);

    void eatFood(Circle & p_food);

    void changeSide();

private:
    const float m_width;
    const float m_height;
    const float m_velocity;
    sf::Vector2f m_direction;
    bool m_isCollision = false;
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> m_directionsAndPositions;
    std::vector<Square> m_blocks;

    bool hit();
    void addTail();
    void reset(Circle& p_food);
};
