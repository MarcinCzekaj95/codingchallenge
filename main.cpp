#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "GameWindow.h"
#include "Snake.h"
#include "Circle.h"
#include "Square.h"
#include "Input.h"

int main()
{
    const float l_sideBarSize = 100;
    const float l_windowWidth = 300;
    const float l_windowHeight = 200;
    GameWindow l_window(l_windowWidth, l_windowHeight, l_sideBarSize, "Snake The Game", sf::Style::Default);
    Snake l_snake(10.0f, 10.0f);

    Circle l_circle(4.0f, 0.0f, 0.0f);
    sf::RectangleShape l_rect;
    l_rect.setSize(sf::Vector2f{ l_windowWidth, l_sideBarSize});
    l_rect.setPosition(0.0f, 0.0f);
    l_rect.setFillColor(sf::Color::Red);
    l_rect.setOutlineThickness(0.6f);

    sf::Text l_text;
    sf::Font l_font;

    if (!l_font.loadFromFile("arial.ttf"))
    {

    }

    l_text.setFont(l_font);

    l_text.setCharacterSize(24);

    l_text.setPosition(50.0f, 50.0f);

    l_text.setFillColor(sf::Color::White);

    l_text.setStyle(sf::Text::Bold | sf::Text::Underlined);

    l_window.getWindow().setFramerateLimit(15);

    while (l_window.getWindow().isOpen())
    {
        sf::Vector2f l_currentPosition = l_snake.getRootBlockPosition();
        Input::buttonsClick(l_window.getWindow());

        if (Input::s_isGoingToRight)
        {
            sf::Vector2f l_direction = sf::Vector2f(1.0f, 0.0f);
            l_snake.savePrevoiusPosition(l_currentPosition, l_direction);
            l_snake.setDirection(l_direction.x, l_direction.y);
            Input::s_isGoingToRight = false;
        }
        if (Input::s_isGoingToLeft)
        {
            sf::Vector2f l_direction = sf::Vector2f(-1.0f, 0.0f);
            l_snake.savePrevoiusPosition(l_currentPosition, l_direction);
            l_snake.setDirection(l_direction.x, l_direction.y);
            Input::s_isGoingToLeft = false;
        }
        if (Input::s_isGoingToUp)
        {
            sf::Vector2f l_direction = sf::Vector2f(0.0f, 1.0f);
            l_snake.savePrevoiusPosition(l_currentPosition, l_direction);
            l_snake.setDirection(l_direction.x, l_direction.y);
            Input::s_isGoingToUp = false;
        }
        if (Input::s_isGoingToDown)
        {
            sf::Vector2f l_direction = sf::Vector2f(0.0f, -1.0f);
            l_snake.savePrevoiusPosition(l_currentPosition, l_direction);
            l_snake.setDirection(l_direction.x, l_direction.y);
            Input::s_isGoingToDown = false;
        }

        l_window.getWindow().clear(sf::Color::Color(30, 30, 30, 255));
        for(auto& shape : l_snake.getBlocks())
            l_window.getWindow().draw(shape.getShape());
        l_window.getWindow().draw(l_circle.getShape());
        l_window.getWindow().draw(l_rect);
        std::stringstream l_stringStream;
        l_stringStream << Square::m_points;
        l_text.setString(l_stringStream.str());
        l_window.getWindow().draw(l_text);

        l_snake.eatFood(l_circle);
        l_snake.update(l_circle);

        l_window.getWindow().display();
    }

    return 0;
}