#include "Snake.h"
#include <time.h>
#include "Input.h"  
#include <iostream>

Snake::Snake(float p_width, float p_height)
    :m_width(p_width), m_height(p_height), m_direction(1.0f, 0.0f), m_velocity(m_width)
{
    srand(time(NULL));
    int l_widonwWidth = GameWindow::getWitdth();
    int l_windowHeight = (GameWindow::getHeight() - GameWindow::getSidebarSize()) + 200;
    m_blocks.push_back(Square{m_width,
                              m_height,
                              rand() % (l_widonwWidth / 20) * 10.0f,
                              (rand() % (l_windowHeight / 20) * 10.0f) + GameWindow::getSidebarSize(), m_direction });
    for(int i = 0; i < 2; ++i)
        addTail();
}

void Snake::update(Circle& p_food)
{
    if (m_direction.x == 0.0f)
    {
        Input::s_horizontalMove = false;
        Input::s_verticalMove = true;
    }
    else if (m_direction.y == 0.0f)
    {
        Input::s_horizontalMove = true;
        Input::s_verticalMove = false;
    }
    if (hit())
    {
        reset(p_food);
    }
    sf::Vector2f l_currentPosition = m_blocks[0].getShape().getPosition();
    m_blocks[0].setPosition(l_currentPosition.x + m_velocity * m_direction.x,
                            l_currentPosition.y + m_velocity * -m_direction.y);

    bool l_shouldPopFromFront = false;
    for (int i = 1; i < m_blocks.size(); ++i)
    {
        sf::Vector2f l_currentPosition = m_blocks[i].getShape().getPosition();
        for (auto& pos : m_directionsAndPositions)
        {
            sf::Vector2f l_blockDirection = m_blocks[i].getDirection();
            if (pos.first == l_currentPosition)
            {
                if (i == m_blocks.size() - 1)
                    l_shouldPopFromFront = true;
                m_blocks[i].setDirection(pos.second);
            }
        }
        sf::Vector2f l_blockDirection = m_blocks[i].getDirection();
        m_blocks[i].setPosition(l_currentPosition.x + m_velocity * l_blockDirection.x,
                                l_currentPosition.y + m_velocity * -l_blockDirection.y);
    }

    if(l_shouldPopFromFront)
    {
        l_shouldPopFromFront = false;
        m_directionsAndPositions.erase(m_directionsAndPositions.begin());
    }

    changeSide();
}

void Snake::reset(Circle& p_food)
{
    m_blocks.clear();
    m_directionsAndPositions.clear();
    Input::s_isGoingToRight = false;
    Input::s_isGoingToLeft = false;
    Input::s_isGoingToUp = false;
    Input::s_isGoingToDown = false;
    Input::s_horizontalMove = true;
    Input::s_verticalMove = false;
    m_direction = {1.0f, 0.0f};
    srand(time(NULL));
    int l_widonwWidth = GameWindow::getWitdth();
    int l_windowHeight = (GameWindow::getHeight() - GameWindow::getSidebarSize());
    m_blocks.push_back(Square{m_width,
                              m_height,
                              rand() % (l_widonwWidth / 20) * 10.0f,
                              (rand() % (l_windowHeight / 20) * 10.0f) + GameWindow::getSidebarSize(), m_direction });
    for (int i = 0; i < 2; ++i)
        addTail();
    p_food.changePosition();
    Square::m_points = 0;
}

bool Snake::hit()
{
    for (int i = 1; i < m_blocks.size(); ++i)
    {
        if (m_blocks[i].getPosition() == getRootBlock().getPosition())
        {
            return true;
        }
    }
    return false;
}

void Snake::eatFood(Circle& p_food)
{
    if(getRootBlock().getPosition() == p_food.getPosition())
    {
        Square::m_points++;
        p_food.changePosition();
        addTail();
    }
}

void Snake::changeSide()
{
    for (auto& snakeBlocks : m_blocks)
    {
        sf::Vector2f l_blockCurrentPosition = snakeBlocks.getPosition();
        if (l_blockCurrentPosition.x > GameWindow::getWitdth() - m_velocity)
        {
            snakeBlocks.setPosition(0.0f, l_blockCurrentPosition.y);
        }
        if (l_blockCurrentPosition.x < 0.0f)
        {
            snakeBlocks.setPosition(GameWindow::getWitdth() - m_velocity, l_blockCurrentPosition.y);
        }
        if (l_blockCurrentPosition.y > GameWindow::getHeight() - m_velocity)
        {
            snakeBlocks.setPosition(l_blockCurrentPosition.x, GameWindow::getSidebarSize());
        }
        if (l_blockCurrentPosition.y < GameWindow::getSidebarSize())
        {
            snakeBlocks.setPosition(l_blockCurrentPosition.x, GameWindow::getHeight() - m_velocity);
        }
    }
}

void Snake::addTail()
{
    auto l_lastBlock = m_blocks.end() - 1;
    sf::Vector2f l_currentPosition = l_lastBlock->getPosition();
    sf::Vector2f l_currentDirection = l_lastBlock->getDirection();
    m_blocks.emplace_back(Square{ m_width, m_height,
        l_currentPosition.x + m_width * -l_currentDirection.x,
        l_currentPosition.y + m_height * l_currentDirection.y,
        l_currentDirection});
}

void Snake::setDirection(float x, float y)
{
    m_direction.x = x;
    m_direction.y = y;
}

void Snake::savePrevoiusPosition(sf::Vector2f p_position, sf::Vector2f p_direction)
{
   m_directionsAndPositions.emplace_back(std::make_pair(p_position, p_direction));
}

std::vector<Square> Snake::getBlocks() const
{
    return m_blocks;
}

Square Snake::getRootBlock() const
{
    return m_blocks[0];
}

sf::Vector2f Snake::getRootBlockPosition() const
{
    return m_blocks[0].getPosition();
}

void Snake::setCollision(bool p_isCollision)
{
    m_isCollision = p_isCollision;
}