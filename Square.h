#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
class Square
{
public:
    Square(float p_width, float p_height, float p_positionX, float p_positionY, sf::Vector2f p_direction);

    void setPosition(float p_positionX, float p_positionY);

    void setFillColor(sf::Color p_color);

    void setDirection(sf::Vector2f p_newDirection);

    sf::Vector2f getDirection() const;

    sf::Vector2f getPosition() const;

    sf::RectangleShape getShape() const;

    static int m_points;

private:
    float m_width;
    float m_height;
    float m_positionX;
    float m_positionY;
    sf::Vector2f m_direction;
    sf::RectangleShape m_shape;
};

