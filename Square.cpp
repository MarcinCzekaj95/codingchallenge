#include "Square.h"

int Square::m_points = 0;

Square::Square(float p_width, float p_height, float p_positionX, float p_positionY, sf::Vector2f p_direction)
    : m_width(p_width), m_height(p_height), m_positionX(p_positionX), m_positionY(p_positionY), m_direction(p_direction)
{
    m_shape.setSize(sf::Vector2f{m_width, m_height});
    m_shape.setPosition(m_positionX, m_positionY);
    m_shape.setOutlineColor(sf::Color::Black);
    m_shape.setOutlineThickness(0.7f);
}

void Square::setPosition(float p_positionX, float p_positionY)
{
    m_shape.setPosition(p_positionX, p_positionY);
}

void Square::setFillColor(sf::Color p_color)
{
    m_shape.setFillColor(p_color);
}

void Square::setDirection(sf::Vector2f p_newDirection)
{
    m_direction = p_newDirection;
}

sf::Vector2f Square::getDirection() const
{
    return m_direction;
}

sf::Vector2f Square::getPosition() const
{
    return m_shape.getPosition();
}

sf::RectangleShape Square::getShape() const
{
    return m_shape;
}
