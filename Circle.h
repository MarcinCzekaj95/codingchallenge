#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include "GameWindow.h"

class Circle
{
public:
    Circle(float p_radius, float p_posX, float p_posY);

    void changePosition();

    sf::CircleShape getShape() const;

    sf::Vector2f getPosition() const;
private:
    sf::CircleShape m_shape;
};

