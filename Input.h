#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
class Input
{
public:

    static void buttonsClick(sf::RenderWindow& p_window);

    static bool s_isGoingToRight;
    static bool s_isGoingToLeft;
    static bool s_isGoingToUp;
    static bool s_isGoingToDown;
    static bool s_horizontalMove;
    static bool s_verticalMove;
    
};

